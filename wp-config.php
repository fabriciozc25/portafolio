<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'fabricioportfolio' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Yq#?uJQj2jS~73/a+-^]-d|@SDKc0q<2,HG]/+N0WtmH8u`Cy@b+X?b$?GApbbv]' );
define( 'SECURE_AUTH_KEY',  '>R KH4DQ0~(olkQ`sVM|oiYE#Ygbs1OPvNIV[[.Dm?,NTW/cB 4Mk.J=?j}D&7Ve' );
define( 'LOGGED_IN_KEY',    'oR&oKNGKs!r3qv])4A3Iz,GW<91wWLxRTvzoHkk;p*&UCJA 2s@DQCqdI-+{@E5.' );
define( 'NONCE_KEY',        '6Hs}y@}*VyE&L2Q>6446BPfF9ygi=0eHz43Zj],-1X2{;lbm@yEpA1*lSwI;Yo@_' );
define( 'AUTH_SALT',        'U>m@s/9SkE<3i;rU>?}T4O}rj4vN]>?JNvI{~pC:<:)Jf 1OMS8^]b@H=m<kF@.E' );
define( 'SECURE_AUTH_SALT', '+rccXQ*wTfFM>Z84%D.@0.pXnrF?&@7}8|cU*B8o3EE-TH-&CcN!R9;/X8Yw?4nV' );
define( 'LOGGED_IN_SALT',   '!e^nv_ bf(%.u$@O_nw~@tV|Egu$mQA.4Q,5(}!{=CAQcZyDV&8Fr/?..W|IGXX~' );
define( 'NONCE_SALT',       'G>eEhRXLo] S /7Wx|K$ (&9%s8o;aAU=2_EtShrVs_c(gg#.`(|ri!r{{%]1<X#' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
