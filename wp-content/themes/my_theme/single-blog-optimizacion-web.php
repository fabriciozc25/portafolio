<?php
/*
Template Name: Plantilla Personalizada para Proyecto Especial
Template Post Type: proyecto
*/
get_header();
?>

<div class="content" style="width: 100%; margin: 0; padding: 0;">
    <?php
    // Verifica si Elementor está activo
    if ( \Elementor\Plugin::$instance->editor->is_edit_mode() ) :
        // Muestra el contenido para la edición de Elementor
        while ( have_posts() ) : the_post();
            the_content();
        endwhile;
    else :
        // Muestra el contenido normalmente
        if ( have_posts() ) :
            while ( have_posts() ) : the_post();
                ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?> style="width: 100%; margin: 0; padding: 0;">
                    <div class="entry-content" style="width: 100%; margin: 0; padding: 0;">
                        <?php the_content(); ?>
                    </div><!-- .entry-content -->
                </article><!-- #post-<?php the_ID(); ?> -->
                <?php
            endwhile;
        endif;
    endif;
    ?>
</div><!-- .content -->

<?php get_footer(); ?>
