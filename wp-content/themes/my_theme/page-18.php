<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Astra
 * @since 1.0.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

get_header(); ?>
<?php if (astra_page_layout() == 'left-sidebar'): ?>

    <?php get_sidebar(); ?>

<?php endif ?>
<div id="primary" <?php astra_primary_class(); ?>>
    <?php
    astra_primary_content_top();

    $current_page = isset($_GET['page']) ? $_GET['page'] : 1;

    $args = [
        'post_type' => 'blog',
        'posts_per_page' => 10,
        'paged' => 1,
    ];

    $query = new WP_Query($args);
    ?>
    <div class="container-post">
        <h1 class="title-post">Blog</h1>

        <div class="card__container">
            <?php
            while ($query->have_posts()) {
                $query->the_post();
                global $post;
                ?>
                <div class="">
                    <a href="<?php echo get_the_permalink(); ?>">
                        <div class="card">
                            <?php $foto = get_field('foto'); ?>
                            <div class="card__cover" style="background-image: url('<?php echo $foto['url'] ?>');"
                                alt="<?php echo $foto['alt'] ?>"></div>
                            <div class="card__body">
                                <div class="card__title"><?php echo get_field('title') ?></div>
                                <div class="card__desc"><?php echo get_field('description') ?></div>
                            </div>
                        </div>
                    </a>
                </div>
                <?php
            }
            wp_reset_postdata();
            ?>
        </div>
    </div>
    <?php
    astra_primary_content_bottom();
    ?>
</div>
<!-- #primary -->
<?php

if (astra_page_layout() == 'right-sidebar'):

    get_sidebar();

endif;

get_footer();
