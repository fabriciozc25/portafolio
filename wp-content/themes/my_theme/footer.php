<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Astra
 * @since 1.0.0
 */

if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly.
}
?>

<?php astra_content_bottom(); ?>

</div><!-- .ast-container -->
</div><!-- #content -->

<p?php
astra_content_after();
astra_footer_before();
?>



<div class="footer-copyright">
	<p>© 2024 Todos los derechos reservados. FabricioDev</p>
</div>


<?php
astra_footer();
astra_footer_after();
?>

</div><!-- #page -->

<?php
astra_body_bottom();
wp_footer();
?>

</body>

</html>