<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Astra
 * @since 1.0.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

get_header(); ?>
<?php if (astra_page_layout() == 'left-sidebar'): ?>

    <?php get_sidebar(); ?>

<?php endif ?>
<div style="width: 100%; margin: 0; padding: 0; id="primary" <?php astra_primary_class(); ?>>
    <?php
    astra_primary_content_top();

    $current_page = isset($_GET['page']) ? $_GET['page'] : 1;

    $args = [
        'post_type' => 'proyecto',
        'posts_per_page' => 10,
        'paged' => $current_page,
    ];

    $query = new WP_Query($args);
    ?>
    <div class="container-post">
        <h1 class="title-post">Proyectos</h1>

        <div class="posts-grid">
            <?php
            while ($query->have_posts()) {
                $query->the_post();
                global $post;
                ?>
                <div class="post-item">
                    <a href="<?php echo get_the_permalink(); ?>">
                        <div class="post-content">
                            <?php
                            // Obtener el campo personalizado 'foto'
                            $foto = get_field('foto', $post->ID);
                            if ($foto):
                                ?>
                                <img class="foto-personal" src="<?php echo esc_url($foto['url']); ?>"
                                    alt="<?php echo esc_attr($foto['alt']); ?>">
                            <?php endif; ?>
                        </div>
                    </a>
                </div>
                <?php
            }
            wp_reset_postdata();
            ?>
        </div>
    </div>
    <?php
    astra_primary_content_bottom();
    ?>
</div>
<!-- #primary -->
<?php

if (astra_page_layout() == 'right-sidebar'):

    get_sidebar();

endif;

get_footer();