<?php

if (!defined('ABSPATH')) exit;

// Encolar estilos personalizados
function my_theme_styles(){
    wp_enqueue_style('my_styles', get_stylesheet_directory_uri().'/styles/css/styles.css');
}
add_action('wp_enqueue_scripts', 'my_theme_styles');

// Encolar scripts y estilos adicionales
function my_theme_scripts() {
    wp_enqueue_script('slickNavScript', 'https://cdnjs.cloudflare.com/ajax/libs/SlickNav/1.0.10/jquery.slicknav.min.js', array('jquery'), '1.0.10', true);
    wp_enqueue_style('slickNavStyles', 'https://cdnjs.cloudflare.com/ajax/libs/SlickNav/1.0.10/slicknav.min.css', array(), '1.0.10', false);
    wp_enqueue_script('my-script', get_stylesheet_directory_uri() . '/scripts/my-script.js', array('slickNavScript'), '1.0.0', true);
}
add_action('wp_enqueue_scripts', 'my_theme_scripts');

// Registrar menús de navegación
register_nav_menus(array(
    'main-menu' => esc_html__('Main menu', 'my_theme'),
    'footer-menu' => esc_html__('Footer menu', 'my_theme'),
    'social-menu' => esc_html__('Social menu', 'my_theme')
));

// Soporte para el logo personalizado
function my_theme_logo_setup(){
    $defaults = array(
        'flex-height' => true,
        'flex-width' => true,
        'header-text' => array('site-title', 'site-description'),
    );
    add_theme_support('custom-logo', $defaults);
}
add_action('after_setup_theme', 'my_theme_logo_setup');

// Limpiar el HTML del logo personalizado
function my_theme_logo_class($html) {
    $html = preg_replace('/(width|height)="\d+"\s/', '', $html);
    return $html;
}
add_filter('get_custom_logo', 'my_theme_logo_class');

// Encolar estilos del tema hijo y del tema padre
function my_theme_enqueue_styles() {
    $parent_style = 'parent-style'; // Este es el estilo del tema padre.

    wp_enqueue_style($parent_style, get_template_directory_uri() . '/style.css');
    wp_enqueue_style('child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array($parent_style)
    );
}
add_action('wp_enqueue_scripts', 'my_theme_enqueue_styles');

function enqueue_google_fonts() {
    wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css2?family=Jost:wght@100..900&display=swap" rel="stylesheet" rel="stylesheet');
}
add_action('wp_enqueue_scripts', 'enqueue_google_fonts');

 